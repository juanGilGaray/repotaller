var productosObtenidos;

function getCustomers() {
//https://services.odata.org/V4/Northwind/Northwind.svc/Products
  var url= "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  //var url= "https://services.odata.org/V4/Northwind/Northwind.svc/Customers/";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //var resultado = request.responseText;
      //console.log(resultado.filter(resultado => Country == "Germany"));
      //console.log(resultado);
      productosObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();

}
//nombre
//ciudad
//bandera
function procesarClientes() {
  var divTabla = document.getElementById("divTabla");
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);

  var tabla   = document.createElement("table");
  var tblBody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    console.log(JSONProductos.value[i]);

    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ContactName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].Country;

    var columnaStock = document.createElement("td");
    var imagen = document.createElement("IMG");
    var bandera = JSONProductos.value[i].Country;
    if(bandera=="UK"){
      bandera = "United-Kingdom";
    }
    bandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+bandera+".png"
    //<img src="https://image.flaticon.com/icons/svg/722/722036.svg" alt="">
    columnaStock.innerHTML = "<IMG src='"+bandera+"'>";

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    tblBody.appendChild(nuevaFila);
  }
  tabla.appendChild(tblBody);
  divTabla.appendChild(tabla);
}
