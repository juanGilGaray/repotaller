var productosObtenidos;

function getproductos() {
//https://services.odata.org/V4/Northwind/Northwind.svc/Products
//?$filter=UnitPrice gt 15&$orderby=UnitPrice desc&$top=5&$skip=2&$count=true&$select =ProductID,ProductName,UnitPrice
  var url= "https://services.odata.org/V4/Northwind/Northwind.svc/Products/";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarProductos() {
  var divTabla = document.getElementById("divTabla");
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);

  var tabla   = document.createElement("table");
  var tblBody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    console.log(JSONProductos.value[i]);

    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    tblBody.appendChild(nuevaFila);
  }
  tabla.appendChild(tblBody);
  divTabla.appendChild(tabla);
}
